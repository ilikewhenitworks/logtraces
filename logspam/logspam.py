import time
from confluent_kafka import Producer

time.sleep(10)
broker = "kafka:9092"
topic = "logs"

conf = {
    'bootstrap.servers': broker,
    'client.id': 'my_producer'
}
producer = Producer(conf)


def delivery_callback(err, msg):
    if err:
        print(f"Message delivery failed: {err}")
    else:
        print(f"Message delivered to {msg.topic()} [{msg.partition()}]")


if __name__ == "__main__":
    try:
        for i in range(10):
            time.sleep(1)
            msg = "2020-08-25 INFO Something routine"
            producer.produce(topic, value=msg, callback=delivery_callback)
            print(f"Produced: {msg}")
    except Exception as e:
        print(f"Failed to produce message: {e}")
    finally:
        producer.flush()
        print("Producer flushed and closed.")
