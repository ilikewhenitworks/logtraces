// Copyright The OpenTelemetry Authors
// SPDX-License-Identifier: Apache-2.0

package logtracesconnector

import (
	"bufio"
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/pdata/plog"
	"go.opentelemetry.io/collector/pdata/ptrace"
)

type mockTraceConsumer struct {
	traces ptrace.Traces
	err    error
}

func (m *mockTraceConsumer) Capabilities() consumer.Capabilities {
	return consumer.Capabilities{MutatesData: false}
}

func (m *mockTraceConsumer) ConsumeTraces(ctx context.Context, traces ptrace.Traces) error {
	m.traces = traces
	return m.err
}

func TestPseudotraceConsumeLogs(t *testing.T) {
	mockConsumer := &mockTraceConsumer{}
	connector := &pseudotrace{
		traceConsumer:         mockConsumer,
		logToTraceTransformer: newLogToTraceTransformer(),
		StartFunc: func(ctx context.Context, host component.Host) error {
			return nil
		},
		ShutdownFunc: func(ctx context.Context) error {
			return nil
		},
	}

	file, err := os.Open("testdata/simple.log")
	if err != nil {
		t.Fatalf("Failed to open log file: %v", err)
	}
	defer file.Close()

	logs := plog.NewLogs()
	resourceLogs := logs.ResourceLogs().AppendEmpty()
	scopeLogs := resourceLogs.ScopeLogs().AppendEmpty()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		logRecord := scopeLogs.LogRecords().AppendEmpty()
		logRecord.Body().SetStr(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		t.Fatalf("Failed to read log file: %v", err)
	}

	err = connector.ConsumeLogs(context.Background(), logs)
	if err != nil {
		t.Fatalf("Failed to consume logs: %v", err)
	}

	assert.NotNil(t, mockConsumer.traces)
	assert.Equal(t, 1, mockConsumer.traces.ResourceSpans().Len())
	resourceSpans := mockConsumer.traces.ResourceSpans().At(0)
	assert.Equal(t, 1, resourceSpans.ScopeSpans().Len())
	ils := resourceSpans.ScopeSpans().At(0)
	assert.Equal(t, 1, ils.Spans().Len())
	span := ils.Spans().At(0)
	assert.Equal(t, "log-to-trace", span.Name())
}
