// Copyright The OpenTelemetry Authors
// SPDX-License-Identifier: Apache-2.0

package logtracesconnector // import "github.com/open-telemetry/opentelemetry-collector-contrib/connector/logtracesconnector"

import (
	"context"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/connector"
	"go.opentelemetry.io/collector/consumer"
)

func NewFactory() connector.Factory {
	return connector.NewFactory(
		"pseudotrace",
		createDefaultConfig,
		connector.WithLogsToTraces(createLogsToTraces, component.StabilityLevelDevelopment),
	)
}

func createDefaultConfig() component.Config {
	return &Config{}
}

func createLogsToTraces(
	_ context.Context,
	set connector.CreateSettings,
	cfg component.Config,
	nextConsumer consumer.Traces,
) (connector.Logs, error) {
	return &pseudotrace{
		traceConsumer: nextConsumer,
	}, nil
}
