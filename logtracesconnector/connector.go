// Copyright The OpenTelemetry Authors
// SPDX-License-Identifier: Apache-2.0

package logtracesconnector // import "github.com/open-telemetry/opentelemetry-collector-contrib/connector/logtracesconnector"

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/consumer"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/plog"
	"go.opentelemetry.io/collector/pdata/ptrace"
)

const scopeName = "otelcol/logtracesconnector"

// pseudotrace converts well-defined log records
// and emits them as traces onto a traces pipeline.
// Note that being constructed from logs means these
// are only as trustworthy as the logs, and real
// tracing should be implemented where possible.
type pseudotrace struct {
	traceConsumer consumer.Traces
	component.StartFunc
	component.ShutdownFunc

	logToTraceTransformer *logToTraceTransformer
}

type logToTraceTransformer struct {
	// TODO: define the requirements of the traces we want to see
}

func newLogToTraceTransformer() *logToTraceTransformer {
	// Initialize the logToTraceTransformer instance
	return &logToTraceTransformer{}
}

func (t *logToTraceTransformer) transformLogsToTraces(ctx context.Context, ld plog.Logs) (ptrace.Traces, error) {
	traces := ptrace.NewTraces()
	resourceSpans := traces.ResourceSpans().AppendEmpty()
	scopeSpans := resourceSpans.ScopeSpans().AppendEmpty()

	for i := 0; i < ld.ResourceLogs().Len(); i++ {
		rl := ld.ResourceLogs().At(i)
		for j := 0; j < rl.ScopeLogs().Len(); j++ {
			sl := rl.ScopeLogs().At(j)
			for k := 0; k < sl.LogRecords().Len(); k++ {
				logRecord := sl.LogRecords().At(k)
				logBody := logRecord.Body().AsString()

				parts := strings.Fields(logBody)
				fmt.Println(parts)
				if len(parts) < 3 {
					// Log format is invalid, skip this record
					continue
				}

				// TODO: no magic numbers!
				timestamp := parts[0]
				//severity := parts[1]
				//message := strings.Join(parts[2:], " ")

				// Parse timestamp
				timestampParsed, err := time.Parse("2006-01-02", timestamp)
				if err != nil {
					continue
				}

				span := scopeSpans.Spans().AppendEmpty()
				span.SetName("log-to-trace")
				span.SetStartTimestamp(pcommon.NewTimestampFromTime(timestampParsed))
				span.SetEndTimestamp(pcommon.NewTimestampFromTime(timestampParsed))
			}
		}
	}

	return traces, nil
}

func (c *pseudotrace) Capabilities() consumer.Capabilities {
	return consumer.Capabilities{MutatesData: true}
}

func (c *pseudotrace) ConsumeLogs(ctx context.Context, ld plog.Logs) error {
	var multiError error
	traces, err := c.logToTraceTransformer.transformLogsToTraces(ctx, ld)
	if err != nil {
		multiError = errors.Join(multiError, err)
	}

	err = c.traceConsumer.ConsumeTraces(ctx, traces)
	if err != nil {
		multiError = errors.Join(multiError, err)
	}

	if multiError != nil {
		return multiError
	}
	return nil

	return c.traceConsumer.ConsumeTraces(ctx, traces)
}
