#+TITLE: 
#+AUTHOR: Garry Cairns
#+EMAIL: garryjcairns@gmail.com
#+STARTUP: hideall
#+LANGUAGE: en
#+OPTIONS: ':t

The commands below will describe, destroy any log topics and recreate them
#+begin_src 
docker exec -it logtracing-kafka-1 kafka-topics --bootstrap-server kafka:9092 --describe
docker exec -it logtracing-kafka-1 kafka-topics --bootstrap-server localhost:9092 --delete --topic logs
docker exec -it logtracing-kafka-1 kafka-topics --bootstrap-server localhost:9092 --create --topic logs --partitions 1 --replication-factor 1
#+end_src

The OpenTelemetry projects describes how one can [[https://opentelemetry.io/docs/specs/otel/logs/][intercept logs]].

[[./demo.gif]]
