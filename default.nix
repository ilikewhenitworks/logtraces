{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "otel-collector-custom";
    src = ./.;
    buildInputs = [
      pkgs.docker-compose
    ];
  }
